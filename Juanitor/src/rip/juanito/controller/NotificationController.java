package rip.juanito.controller;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.ImageIcon;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import rip.juanito.Main;
import rip.juanito.trades.notifications.Notification;
import rip.juanito.trades.notifications.NotificationType;
import javafx.scene.image.Image;
public class NotificationController {

    @FXML
    private Label name;

    @FXML
    private ImageView img;
    @FXML
    private Label pnl;
    @FXML
    private Label description;

    @FXML
    private Label date;

	public void init(Notification notification) {
		name.setText(notification.getCurrency().getName());
		Image  imageObject = new Image(notification.getType().getIcon()); 
		img.setImage(imageObject);
		description.setText(notification.getDescription());
		date.setText(new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date(notification.getDate())));
		if(notification.getType().equals(NotificationType.BUY_DIP)) {
			pnl.setText("Cost:" + new DecimalFormat("00.00").format(notification.getPnl()) + "$");
		}else {
			pnl.setText("PNL:" + new DecimalFormat("00.00").format(notification.getPnl()) + "$");
		}
	}

}
