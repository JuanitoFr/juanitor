package rip.juanito.controller;


import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import rip.juanito.Main;
import rip.juanito.trades.Currency;
import rip.juanito.trades.LocalAccount;
import rip.juanito.trades.notifications.Notification;
import javafx.fxml.Initializable;
import javafx.scene.control.ScrollPane;

@SuppressWarnings("restriction")
public class Controller implements Initializable{

	
    @FXML
    private ScrollPane scroll;
    
    @FXML
    private Label other;

    @FXML
    private Label usdt;

    @FXML
    private VBox coins;
    @FXML
    private Label status;
    
    @FXML
    private Label date;
    
    @FXML
    private Label pnl;
    @FXML
    private VBox notifs;
    
    public static List<CoinController> coinsController = new ArrayList<>();
    
    public void init(List<Currency> coins) throws IOException {
    	int x = 0;
    	for(Currency c : coins) {
    		FXMLLoader fxmlLoader = new FXMLLoader();
			
			Node n = fxmlLoader.load(Main.getInstance().getClass().getResource("coinpane.fxml"));
			this.coins.getChildren().add(n);
			n.setLayoutX(x);
			x+=56;
    	}
    }
    
    private int lastNotif = 0;
    
    public void update() {
    	DecimalFormat df = new DecimalFormat("0");
    	LocalAccount account = Main.getInstance().getLocalAccount();
    	int index = 0;
    	for(Currency c : Main.getInstance().getCurrencies()) {
    		coinsController.get(index).update(c);
    		index++;
    	}
    	
    	
    	Platform.runLater(new Runnable() {
			public void run() {
				int notifsCount = Notification.notifications.size();
				

				if(notifsCount > lastNotif) {
					
					for(int i = lastNotif; i<notifsCount; i++) {
						Node n = Notification.notifications.get(i).getOutput();
						n.setLayoutX(i*45);
						notifs.getChildren().add(n);
						
					}
					lastNotif = notifsCount;
				}
				status.setText(account.isSimulation() ? "Simulation" : "Connected");
				usdt.setText(df.format(account.getUSDT())+"$");
		    	other.setText(df.format(account.getCoinsTotalValue())+"$");
		    	if(Main.getInstance().backtest != null) {
		    		date.setText("Date: " + Main.getInstance().backtest.getDate().toGMTString());
		    		double percent =  ((account.getWalletTotalValue()/1000)-1);
		    		if(percent > 0) {
		    			pnl.setStyle("-fx-text-fill: #00861d");
					}else {
						pnl.setStyle("-fx-text-fill: #b82828");
					}
		    		
		    		pnl.setText("PNL: " + new DecimalFormat("0.0").format(percent*1000)+"$");
		    	}else {
		    		date.setText("Date: " + new Date(System.currentTimeMillis()).toGMTString());
		    		pnl.setVisible(false);
		    	}
		    	scroll.setVvalue(scroll.getVmax()*2);
			}
			});
    	
    	
    }

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		Main.getInstance().controller = this;	
		try {
			init(Main.getInstance().getCurrencies());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}