package rip.juanito.controller;


import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;

import com.fasterxml.jackson.annotation.JacksonInject.Value;

import javafx.fxml.FXML;
import javafx.application.Platform;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import rip.juanito.Main;
import rip.juanito.api.CurrentAPI;
import rip.juanito.trades.Currency;

public class CoinController implements Initializable{


    @FXML
    public Label name;
    
    @FXML
    public Label price;

    @FXML
    public ImageView img;

    @FXML
    public Label amount;
    
    @FXML
    public Label value;
    
    boolean first = true;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		Main.getInstance().controller.coinsController.add(this);
		
	}
	
	private long lastDayUpdate;
	
	public void update(Currency c) {
		Platform.runLater(new Runnable() {
			public void run() {
				
				DecimalFormat df = new DecimalFormat("0.00");
				if(Main.getInstance().backtest == null) {
					if(System.currentTimeMillis() - lastDayUpdate > 1000*60) {
						double percent = Double.parseDouble(CurrentAPI.get().get24HrPriceStatistics(c.getPair()).getPriceChangePercent());
						if(percent > 0) {
							price.setStyle("-fx-text-fill: #00861d");
						}else {
							price.setStyle("-fx-text-fill: #b82828");
						}
						
						lastDayUpdate = System.currentTimeMillis();
					}
				}
				
				Image  imageObject = new Image(getIcon(c)); 
				img.setImage(imageObject);
				name.setText(c.getName());
				price.setText(df.format(c.getPrice()) +"$");
				double a = Main.getInstance().getLocalAccount().getWallet().get(c) ;
				value.setText(df.format(c.getPrice()*a) + "$");

				amount.setText(df.format(a) +  " " + c.getName());
			}
			
				});
		}
	
	public String getIcon(Currency c) {
		return "\"../../resources/icon/"+c.getName()+".png";
	}
		
	}

