package rip.juanito.config;

public class ConfigException extends Exception {
    public ConfigException(String message) {
        super(message);
    }
}
