package rip.juanito.trades;

import java.io.Closeable;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import com.binance.api.client.BinanceApiWebSocketClient;
import com.binance.api.client.domain.account.NewOrderResponse;
import com.binance.api.client.domain.market.Candlestick;
import com.binance.api.client.domain.market.CandlestickInterval;
import com.fasterxml.jackson.annotation.JsonCreator.Mode;

import rip.juanito.Main;
import rip.juanito.api.CurrentAPI;
import rip.juanito.config.ConfigSetup;
import rip.juanito.config.Formatter;
import rip.juanito.indicators.DBB;
import rip.juanito.indicators.Indicator;
import rip.juanito.indicators.MACD;
import rip.juanito.indicators.RSI;
import rip.juanito.trades.data.PriceBean;
import rip.juanito.trades.data.PriceReader;
import rip.juanito.trades.notifications.Notification;
import rip.juanito.trades.notifications.NotificationType;

public class Currency implements Closeable {

	public static int CONFLUENCE_TARGET = 2;

	private final String pair;
	private long candleTime;
	private final List<Indicator> indicators = new ArrayList<>();
	private final AtomicBoolean currentlyCalculating = new AtomicBoolean(false);
	private double currentPrice;
	private double entryPrice = -1;
	double lastSL;
	private long currentTime;

	// Backtesting data
	private final StringBuilder log = new StringBuilder();
	private PriceBean firstBean;

	private Closeable apiListener;
	private String name;

	private List<Trade> tradeHistory = new ArrayList<>();

	// Used for SIMULATION and LIVE
	public Currency(String coin, boolean backtest) {
		this.name = coin;
		this.pair = coin + ConfigSetup.getFiat();

		if (!backtest) {
			// Every currency needs to contain and update our indicators
			List<Candlestick> history = CurrentAPI.get().getCandlestickBars(pair, CandlestickInterval.HOURLY);
			List<Double> closingPrices = history.stream().map(candle -> Double.parseDouble(candle.getClose()))
					.collect(Collectors.toList());

			indicators.add(new RSI(closingPrices, 14));
			indicators.add(new MACD(closingPrices, 12, 26, 9));
			indicators.add(new DBB(closingPrices, 20));

			// We set the initial values to check against in onMessage based on the latest
			// candle in history
			currentTime = System.currentTimeMillis();
			candleTime = history.get(history.size() - 1).getCloseTime();
			currentPrice = Double.parseDouble(history.get(history.size() - 1).getClose());

			BinanceApiWebSocketClient client = CurrentAPI.getFactory().newWebSocketClient();
			// We add a websocket listener that automatically updates our values and
			// triggers our strategy or trade logic as needed
			apiListener = client.onAggTradeEvent(pair.toLowerCase(), response -> {
				// Every message and the resulting indicator and strategy calculations is
				// handled concurrently
				// System.out.println(Thread.currentThread().getId());
				double newPrice = Double.parseDouble(response.getPrice());
				long newTime = response.getEventTime();

				// We want to toss messages that provide no new information
				if (currentPrice == newPrice && newTime <= candleTime) {
					return;
				}

				if (newTime > candleTime) {
					accept(new PriceBean(candleTime, currentPrice, true));
					candleTime += 300000L;
				}

				accept(new PriceBean(newTime, newPrice));
			});
		}
		System.out.println("---SETUP DONE FOR " + this);
	}

	public List<Indicator> getIndicators() {
		return indicators;
	}

	public List<Trade> getTradeHistory() {
		return tradeHistory;
	}
	
	private long lastBuy = 0;

	public void accept(PriceBean bean) {
		// Make sure we dont get concurrency issues
		if (currentlyCalculating.get()) {
			System.out.println("------------WARNING, NEW THREAD STARTED ON " + pair
					+ " MESSAGE DURING UNFINISHED PREVIOUS MESSAGE CALCULATIONS");
		}

		currentPrice = bean.getPrice();

		currentTime = bean.getTimestamp();

		if (bean.isClosing()) {
			indicators.forEach(indicator -> indicator.update(bean.getPrice()));
		}

		if (!currentlyCalculating.get()) {
			int confluence = 0; // 0 Confluence should be reserved in the config for doing nothing
			currentlyCalculating.set(true);

			confluence = check();
			if (confluence >= CONFLUENCE_TARGET && BuySell.enoughFunds() && Main.getInstance().getLocalAccount().getWallet().get(this)>0) {
				System.out.println("OPPORTUNITY TO BUY " + getName() + " at " + currentPrice);
				LocalAccount account = Main.getInstance().getLocalAccount();
				if (account.getPercentOfWallet(this) <= LocalAccount.PER_CRYPTO_ALLOCATION && currentTime - lastBuy > 3600*1000*6) {
					double count = (account.getUSDT()*LocalAccount.PER_CRYPTO_ALLOCATION) / currentPrice;
					double midPrice = ((account.getWallet().get(this) * entryPrice) + (count * getPrice())) / (count
							+ account.getWallet().get(this));
					this.entryPrice = midPrice;
					buy(count, "BUY SIGNAL");
					lastBuy = currentTime;

				}
			}
			if (currentPrice > entryPrice * 1.5
					&& Main.getInstance().getLocalAccount().getPercentOfWallet(this) > LocalAccount.PER_CRYPTO_ALLOCATION) {
				sell(Main.getInstance().getLocalAccount().getWallet().get(this)*0.1,
						"TP & " + Main.getInstance().getLocalAccount().getPercentOfWallet(this) + "%");
				entryPrice = entryPrice *1.4;

			}
			currentlyCalculating.set(false);
		}
	}

	public int check() {
		return indicators.stream().mapToInt(indicator -> indicator.check(currentPrice)).sum();
	}

	public int getRSI() {
		for (Indicator i : indicators) {
			if (i.getClass().equals(RSI.class)) {
				return i.check(this.currentPrice);
			}
		}
		return 0;
	}

	public String getExplanations() {
		StringBuilder builder = new StringBuilder();
		for (Indicator indicator : indicators) {
			String explanation = indicator.getExplanation();
			if (explanation == null)
				explanation = "";
			builder.append(explanation.equals("") ? "" : explanation + "\t");
		}
		return builder.toString();
	}

	public String getPair() {
		return pair;
	}

	public double getPrice() {
		return currentPrice;
	}

	public long getCurrentTime() {
		return currentTime;
	}

	public void appendLogLine(String s) {
		log.append(s).append("\n");
	}

	public boolean sell(double amount, String explanation) {
		LocalAccount localAccount = Main.getInstance().getLocalAccount();
		Trade trade;
		if (!localAccount.isSimulation()) {
			NewOrderResponse order = BuySell.placeOrder(this, amount, false);
			if (order == null) {
				return false;
			}
			double fillsQty = 0;
			double fillsPrice = 0;
			for (com.binance.api.client.domain.account.Trade fill : order.getFills()) {
				double qty = Double.parseDouble(fill.getQty());
				fillsQty += qty;
				fillsPrice += qty * Double.parseDouble(fill.getPrice()) - Double.parseDouble(fill.getCommission());
			}

			localAccount.removeFromWallet(this, fillsQty);
			localAccount.addUSDT(fillsPrice);
			trade = new Trade(this, fillsPrice / fillsQty, amount, explanation, TradeType.SELL);
			System.out.println(
					"I am selling " + amount + " " + getName() + " at " + Formatter.formatDate(order.getTransactTime())
							+ ", at a price of " + Formatter.formatDecimal(fillsPrice) + " USDT");
		} else {

			localAccount.removeFromWallet(this, amount);
			localAccount.addUSDT(amount * currentPrice);
			trade = new Trade(this, currentPrice / amount, amount, explanation, TradeType.SELL);
			System.out.println(
					"I am selling " + amount + " " + getName() + " at " + Formatter.formatDate(getCurrentTime())
							+ ", at a price of " + Formatter.formatDecimal(currentPrice) + " USDT");
		}
		this.tradeHistory.add(trade);
		DecimalFormat df = new DecimalFormat("0.00");

		new Notification(NotificationType.TAKE_PROFIT, this, "SELL " + df.format(amount) + " " + getName() + " AT " + df.format(currentPrice) , getCurrentTime(), amount*currentPrice).show();
		return true;

	}

	public void buy(double amount, String explanation) {
		LocalAccount localAccount = Main.getInstance().getLocalAccount();
		double oldQty = localAccount.getWallet().get(this);
		if (!BuySell.enoughFunds()) {
			System.out.println(
					"---Out of funds, cannot open trade! (" + Formatter.formatDecimal(localAccount.getUSDT()) + ")");
			return;
		}

		double currentPrice = getPrice(); // Current price of the currency
		double fiatCost = amount * currentPrice;
		Trade trade;

		if (!localAccount.isSimulation()) {
			NewOrderResponse order = BuySell.placeOrder(this, amount, true);
			if (order == null) {
				return;
			}
			double fillsQty = 0;
			double fillsPrice = 0;

			for (com.binance.api.client.domain.account.Trade fill : order.getFills()) {
				double qty = Double.parseDouble(fill.getQty());
				fillsQty += qty - Double.parseDouble(fill.getCommission());
				fillsPrice += qty * Double.parseDouble(fill.getPrice());
			}
			fiatCost = fillsPrice;
			amount = fillsQty;
			trade = new Trade(this, fillsPrice / fillsQty, amount, explanation, TradeType.BUY);
			System.out.println(
					"I am buying " + amount + " " + getName() + " at " + Formatter.formatDate(order.getTransactTime())
							+ ", at a price of " + Formatter.formatDecimal(fillsPrice) + " USDT");

		} else {
			trade = new Trade(this, currentPrice, amount, explanation, TradeType.BUY);
			System.out
					.println("I am buying " + amount + " " + getName() + " at " + Formatter.formatDate(getCurrentTime())
							+ ", at a price of " + Formatter.formatDecimal(currentPrice) + " USDT");

		}
		// Converting fiat value to coin value
		localAccount.addUSDT(-fiatCost);
		localAccount.addToWallet(this, amount);
		getTradeHistory().add(trade);
		DecimalFormat df = new DecimalFormat("0.00");
		new Notification(NotificationType.BUY_DIP, this, "BUY " + df.format(amount) + " " + getName() + " AT " + df.format(currentPrice) , getCurrentTime(), fiatCost).show();
		if (this.entryPrice == -1) {
			this.entryPrice = currentPrice;
		}
		System.out.println("New entry price for " + getName() + ": " + entryPrice);

	}

	public void log(String path) {
		List<Trade> tradeHistory = new ArrayList<>(getTradeHistory());
		try (FileWriter writer = new FileWriter(path)) {
			writer.write("Test ended " + Formatter.formatDate(LocalDateTime.now()) + " \n");
			writer.write("\n\nCONFIG:\n");
			writer.write(ConfigSetup.getSetup());
			writer.write("\n\nMarket performance: "
					+ Formatter.formatPercent((currentPrice - firstBean.getPrice()) / firstBean.getPrice()));
			if (!tradeHistory.isEmpty()) {
				tradeHistory.sort(Comparator.comparingDouble(Trade::getProfit));
				double maxLoss = tradeHistory.get(0).getProfit();
				double maxGain = tradeHistory.get(tradeHistory.size() - 1).getProfit();
				int lossTrades = 0;
				double lossSum = 0;
				int gainTrades = 0;
				double gainSum = 0;
				long tradeDurs = 0;
				for (Trade trade : tradeHistory) {
					double profit = trade.getProfit();
					if (profit < 0) {
						lossTrades += 1;
						lossSum += profit;
					} else if (profit > 0) {
						gainTrades += 1;
						gainSum += profit;
					}
					tradeDurs += trade.getDuration();
				}

				double tradePerWeek = 604800000.0
						/ (((double) currentTime - firstBean.getTimestamp()) / tradeHistory.size());

				writer.write(
						"\nBot performance: " + Formatter.formatPercent(BuySell.getAccount().getProfit()) + "\n\n");
				writer.write(getTradeHistory().size() + " closed trades" + " (" + Formatter.formatDecimal(tradePerWeek)
						+ " trades per week) with an average holding length of "
						+ Formatter.formatDuration(Duration.of(tradeDurs / tradeHistory.size(), ChronoUnit.MILLIS))
						+ " hours");
				if (lossTrades != 0) {
					writer.write("\nLoss trades:\n");
					writer.write(lossTrades + " trades, " + Formatter.formatPercent(lossSum / (double) lossTrades)
							+ " average, " + Formatter.formatPercent(maxLoss) + " max");
				}
				if (gainTrades != 0) {
					writer.write("\nProfitable trades:\n");
					writer.write(gainTrades + " trades, " + Formatter.formatPercent(gainSum / (double) gainTrades)
							+ " average, " + Formatter.formatPercent(maxGain) + " max");
				}
				writer.write("\n\nClosed trades (least to most profitable):\n");
				for (Trade trade : tradeHistory) {
					writer.write(trade.toString() + "\n");
				}
			} else {
				writer.write("\n(Not trades made)\n");
				System.out.println("---No trades made in the time period!");
			}
			writer.write("\n\nFULL LOG:\n\n");
			writer.write(log.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("---Log file generated at " + new File(path).getAbsolutePath());
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder(pair + " price: " + currentPrice);
		if (currentTime == candleTime)
			indicators.forEach(indicator -> s.append(", ").append(indicator.getClass().getSimpleName()).append(": ")
					.append(Formatter.formatDecimal(indicator.get())));
		else
			indicators.forEach(indicator -> s.append(", ").append(indicator.getClass().getSimpleName()).append(": ")
					.append(Formatter.formatDecimal(indicator.getTemp(currentPrice))));

		return s.toString();
	}

	@Override
	public int hashCode() {
		return pair.hashCode();
	}
	
	public void setEntryPrice(double entryPrice) {
		this.entryPrice = entryPrice;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj.getClass() != Currency.class)
			return false;
		return pair.equals(((Currency) obj).pair);
	}

	@Override
	public void close() throws IOException {
//		if (Mode.get().equals(Mode.BACKTESTING) || Mode.get().equals(Mode.COLLECTION))
//			return;
		apiListener.close();
	}

	public String getName() {
		return name;
	}
}
