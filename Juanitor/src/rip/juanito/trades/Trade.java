package rip.juanito.trades;

import rip.juanito.config.Formatter;

public class Trade {

	private double high; // Set the highest price

	public static double TRAILING_SL; // It's in percentages, but using double for comfort.
	public static double TAKE_PROFIT; // It's in percentages, but using double for comfort.

	private final long openTime;
	private final double entryPrice; // Starting price of a trade (when logic decides to buy)
	private final Currency currency; // What cryptocurrency is used.
	private double amount; // How much are you buying or selling. I.E 6 bitcoins or smth.
	private String explanation;
	private TradeType type;

	public Trade(Currency currency, double entryPrice, double amount, String explanation, TradeType type) {
		this.currency = currency;
		this.entryPrice = entryPrice;
		this.high = entryPrice;
		this.amount = amount;
		this.explanation = explanation;
		this.type = type;
		openTime = currency.getCurrentTime();
	}
	
	public TradeType getTradeType() {
		return type;
	}

	// Getters and setters

	public String getExplanation() {
		return explanation;
	}

	public void setExplanation(String explanation) {
		this.explanation = explanation;
	}


	public double getEntryPrice() {
		return entryPrice;
	}

	public Currency getCurrency() { // for getting the currency to calculate what the price is now.
		return currency;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}



	public long getOpenTime() {
		return openTime;
	}

	// Allows user to get the profit percentages on one specific trade.
	public double getProfit() {
		return (currency.getPrice() - entryPrice) / entryPrice;

	}

	public long getDuration() {
		return System.currentTimeMillis() - openTime;
	}
	


	public void update(double newPrice, int confluence) {
		if (newPrice > high)
			high = newPrice;
		if (getProfit() > TAKE_PROFIT) {
			explanation += "Closed due to: Take profit";
			// TODO SELL
			return;
		}
		
	}

	@Override
	public String toString() {
		return (currency.getTradeHistory().indexOf(this) + 1) + " " + currency.getPair() + " "
				+ Formatter.formatDecimal(amount) + "\n" + "open: " + Formatter.formatDate(openTime) + " at "
				+ entryPrice + "\n"
				+ "current price: " + currency.getPrice()
				+ "\n" + "high: " + high + ", profit: " + Formatter.formatPercent(getProfit()) + "\n" + explanation
				+ "\n";
	}
}
