package rip.juanito.trades;

import static com.binance.api.client.domain.account.NewOrder.marketBuy;
import static com.binance.api.client.domain.account.NewOrder.marketSell;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Optional;

import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.OrderStatus;
import com.binance.api.client.domain.account.NewOrderResponse;
import com.binance.api.client.domain.account.NewOrderResponseType;
import com.binance.api.client.domain.general.FilterType;
import com.binance.api.client.domain.general.SymbolFilter;
import com.binance.api.client.exception.BinanceApiException;
import com.fasterxml.jackson.annotation.JsonCreator.Mode;

import rip.juanito.api.CurrentAPI;
import rip.juanito.config.Formatter;

public class BuySell {

    private static LocalAccount localAccount;
    public static double MONEY_PER_TRADE;

    public static void setAccount(LocalAccount localAccount) {
        BuySell.localAccount = localAccount;
    }

    public static LocalAccount getAccount() {
        return localAccount;
    }

    private BuySell() {
        throw new IllegalStateException("Utility class");
    }

    public static boolean enoughFunds() {
        return localAccount.getUSDT() > 0;
    }

    //TODO: Implement limit ordering
    public static NewOrderResponse placeOrder(Currency currency, double amount, boolean buy) {
        System.out.println("\n---Placing a " + (buy ? "buy" : "sell") + " market order for " + currency.getPair());
        BigDecimal originalDecimal = BigDecimal.valueOf(amount);
        //Round amount to base precision and LOT_SIZE
        int precision = CurrentAPI.get().getExchangeInfo().getSymbolInfo(currency.getPair()).getBaseAssetPrecision();
        String lotSize;
        Optional<String> minQtyOptional = CurrentAPI.get().getExchangeInfo().getSymbolInfo(currency.getPair()).getFilters().stream().filter(f -> FilterType.LOT_SIZE == f.getFilterType()).findFirst().map(f1 -> f1.getMinQty());
        Optional<String> minNotational = CurrentAPI.get().getExchangeInfo().getSymbolInfo(currency.getPair()).getFilters().stream().filter(f -> FilterType.MIN_NOTIONAL == f.getFilterType()).findFirst().map(SymbolFilter::getMinNotional);
        if (minQtyOptional.isPresent()) {
            lotSize = minQtyOptional.get();
        } else {
            System.out.println("---Could not get LOT_SIZE so could not open trade!");
            return null;
        }
        double minQtyDouble = Double.parseDouble(lotSize);

        //Check LOT_SIZE to make sure amount is not too small
        if (amount < minQtyDouble) {
            System.out.println("---Amount smaller than min LOT_SIZE, could not open trade! (min LOT_SIZE=" + lotSize + ", amount=" + amount);
            return null;
        }

        //Convert amount to an integer multiple of LOT_SIZE and convert to asset precision
        System.out.println("Converting from double trade amount " + originalDecimal.toString() + " to base asset precision " + precision + " LOT_SIZE " + lotSize);
        String convertedAmount = new BigDecimal(lotSize).multiply(new BigDecimal((int) (amount / minQtyDouble))).setScale(precision, RoundingMode.HALF_DOWN).toString();
        System.out.println("Converted to " + convertedAmount);

        if (minNotational.isPresent()) {
            double notational = Double.parseDouble(convertedAmount) * currency.getPrice();
            if (notational < Double.parseDouble(minNotational.get())) {
                System.out.println("---Cannot open trade because notational value " + Formatter.formatDecimal(notational) + " is smaller than minimum " + minNotational.get());
            }
        }

        NewOrderResponse order;
        try {
            BinanceApiRestClient client = CurrentAPI.get();
            order = client.newOrder(
                    buy ?
                            marketBuy(currency.getPair(), convertedAmount).newOrderRespType(NewOrderResponseType.FULL) :
                            marketSell(currency.getPair(), convertedAmount).newOrderRespType(NewOrderResponseType.FULL));
            System.out.println("---Executed a " + order.getSide() + " order with id " + order.getClientOrderId() + " for " + convertedAmount + " " + currency.getPair());
            if (!order.getStatus().equals(OrderStatus.FILLED)) {
                System.out.println("Order is " + order.getStatus() + ", not FILLED!");
            }
            return order;
        } catch (BinanceApiException e) {
            System.out.println("---Failed " + (buy ? "buy" : "sell") + " " + convertedAmount + " " + currency.getPair());
            System.out.println(e.getMessage());
            return null;
        }
    }
}
