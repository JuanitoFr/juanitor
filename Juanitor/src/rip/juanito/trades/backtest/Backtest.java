package rip.juanito.trades.backtest;

import java.sql.Date;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

import com.binance.api.client.BinanceApiRestClient;
import com.binance.api.client.domain.market.Candlestick;
import com.binance.api.client.domain.market.CandlestickInterval;

import rip.juanito.Main;
import rip.juanito.api.CurrentAPI;
import rip.juanito.indicators.DBB;
import rip.juanito.indicators.MACD;
import rip.juanito.indicators.RSI;
import rip.juanito.trades.Currency;
import rip.juanito.trades.LocalAccount;
import rip.juanito.trades.data.PriceBean;

public class Backtest {

	private static final long delay = 1000 * 60 * 60 * 4;
	private static final long DAYS = 1000 * 60 * 60 * 24 * 14;
	private long start;
	private long done;
	BinanceApiRestClient client = CurrentAPI.get();
	private long realStartTime = System.currentTimeMillis();

	public Backtest(long start) {
		this.start = start;
	}

	public void start() {
		while (start + done < (System.currentTimeMillis()-7200*1000)) {
			tick();
		}
		System.out.println("Done. (" + ((System.currentTimeMillis() - realStartTime)/1000) + "s)");
		double percent =  ((Main.getInstance().getLocalAccount().getWalletTotalValue()/1000)-1)*100;

		System.out.println("RDM: "+ new DecimalFormat("0.0").format(percent)+"%" + " (+" + (Main.getInstance().getLocalAccount().getWalletTotalValue()-500) + "$)");
	}
	boolean currentCalulation = false;

	public void tick() {
		if(currentCalulation) return;
		currentCalulation = true;
		if (done > 1) {
			double count = (System.currentTimeMillis() - start) / delay;
			double percent = ((done / delay) * 100) / count;

			System.out.println(percent + "%");
		
		}
		
			for (Currency c : Main.getInstance().getCurrencies()) {
				
				if (done == 0) {
					List<Candlestick> cc = client.getCandlestickBars(c.getPair(), CandlestickInterval.FOUR_HOURLY, 14*6,
							start - DAYS, start);
					List<Double> closingPrices = cc.stream().map(candle -> Double.parseDouble(candle.getClose()))
							.collect(Collectors.toList());
					c.getIndicators().add(new RSI(closingPrices, 14));
					c.getIndicators().add(new MACD(closingPrices, 12, 26, 9));
					c.getIndicators().add(new DBB(closingPrices, 20));
					c.accept(new PriceBean(start + done, closingPrices.get(0)));
					

				} else {
					List<Candlestick> list =  client.getCandlestickBars(c.getPair(), CandlestickInterval.FOUR_HOURLY, 1,
							start + done, start + done + delay);
					if(list==null || list.isEmpty()) continue;
					Candlestick candlestick =list.get(0);
					if (candlestick == null)
						continue;
					
					
				
					c.getIndicators().forEach(p -> p.update(Double.parseDouble(candlestick.getClose())));
					c.accept(new PriceBean(start + done, Double.parseDouble(candlestick.getClose())));
					if (done == 4*delay) {
						c.buy((1000*LocalAccount.PER_CRYPTO_ALLOCATION)/c.getPrice(), "Starting");
					}
					
				}

			}
		
		
		done += delay;
		currentCalulation = false;
	}

	public Date getDate() {
		return new Date(start+done);
	}

}
