package rip.juanito.trades.notifications;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import rip.juanito.Main;
import rip.juanito.controller.NotificationController;
import rip.juanito.trades.Currency;

@SuppressWarnings("restriction")
public class Notification {
	
	public static final int NOTIFICATION_HEIGHT = 50;
	
	public static List<Notification> notifications = new ArrayList<>();
	private NotificationType type;
	private String description;
	private long date;
	private NotificationController notificationController;
	private Currency currency;
	private double pnl;
	
	public Notification(NotificationType type, Currency currency, String description, long date, double pnl) {
		this.type = type;
		this.description = description;
		this.date = date;
		this.currency = currency;
		this.pnl = pnl;
		
	}
	
	public double getPnl() {
		return pnl;
	}
	
	public long getDate() {
		return date;
	}
	public String getDescription() {
		return description;
	}
	
	public void show() {
		notifications.add(this);
	}
	
	public NotificationType getType() {
		return type;
	}
	
	public Currency getCurrency() {
		return currency;
	}
	

	
	public Node getOutput() {
		FXMLLoader fxmlLoader = new FXMLLoader();
		Node n = null;
		try {
			fxmlLoader = new FXMLLoader(Main.getInstance().getClass().getResource("notifications.fxml"));
			n = fxmlLoader.load();
			this.notificationController = (NotificationController)fxmlLoader.getController();
			this.notificationController.init(this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return n;
	}

}
