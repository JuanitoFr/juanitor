package rip.juanito.trades.notifications;

public enum NotificationType {
	
	TAKE_PROFIT("\"../../resources/TAKE_PROFIT.png"),
	BUY_DIP("\"../../resources/BUY_DIP.png");
	
	
	private String icon;
	
	private NotificationType(String icon) {
		this.icon = icon;
	}
	
	public String getIcon() {
		return icon;
	}
	

}
