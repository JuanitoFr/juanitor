package rip.juanito.trades;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import com.binance.api.client.domain.account.Account;

import rip.juanito.Main;
import rip.juanito.api.CurrentAPI;
import rip.juanito.config.ConfigSetup;
import rip.juanito.config.Formatter;

public class LocalAccount {
	
	public final static double PER_CRYPTO_ALLOCATION = 0.15;
	public final boolean simulation = true;

    private final String username;
    private Account realAccount;

    private double startingValue;
    private ConcurrentHashMap<Currency, Double> wallet;
    private double makerComission;
    private double takerComission;
    private double buyerComission;
    private double USDT = 1000;

    public boolean isSimulation() {
		return simulation;
	}
    
    public void addUSDT(double usdt) {
    	this.USDT+=usdt;
    }
    /**
     * Wallet value will most probably be 0 at first, but you could start
     * with an existing wallet value as well.
     */
    public LocalAccount(String username, double startingValue) {
        this.username = username;
        this.startingValue = startingValue;
        USDT = startingValue;
    }

    public LocalAccount(String apiKey, String secretApiKey) {
        CurrentAPI.login(apiKey, secretApiKey);
        username = "Juanito";
        realAccount = CurrentAPI.get().getAccount();
        if (!realAccount.isCanTrade()) {
            System.out.println("Can't trade!");
        }
        makerComission = realAccount.getMakerCommission(); //Maker fees are
        // paid when you add liquidity to our order book
        // by placing a limit order below the ticker price for buy, and above the ticker price for sell.
        takerComission = realAccount.getTakerCommission();//Taker fees are paid when you remove
        // liquidity from our order book by placing any order that is executed against an order on the order book.
        buyerComission = realAccount.getBuyerCommission();

        //Example: If the current market/ticker price is $2000 for 1 BTC and you market buy bitcoins starting at the market price of $2000, then you will pay the taker fee. In this instance, you have taken liquidity/coins from the order book.
        //
        //If the current market/ticker price is $2000 for 1 BTC and you
        //place a limit buy for bitcoins at $1995, then
        //you will pay the maker fee IF the market/ticker price moves into your limit order at $1995.
   }
    
    public void initAccount() {
        updateUSDT();
        updateWallet();
    }
    
    public void updateWallet() {
    	if(wallet == null) {
    		wallet = new ConcurrentHashMap<Currency, Double>();
    		if(simulation) {
    			List<Currency> list = Main.getInstance().getCurrencies();
    			LocalAccount account = Main.getInstance().getLocalAccount();

    			for(Currency c : list) {
    				account.getWallet().put(c, 0.0);
    			}
//    			
//    			// eth
//    			account.getWallet().put(list.get(0), 0.73);
//    			list.get(0).setEntryPrice(3050);
//    			
//    			
//    			// sol
//    			account.getWallet().put(list.get(1), 40.0);
//    			list.get(1).setEntryPrice(80);
//    			
//    			// ADA
//    			account.getWallet().put(list.get(2), 1000.0);
//    			list.get(2).setEntryPrice(2.75);
//    			
//    			// MATIC
//    			account.getWallet().put(list.get(3), 1667.0);
//    			list.get(3).setEntryPrice(1.35);
//    			
//    			// BNB
//    			account.getWallet().put(list.get(4), 4.8);
//    			list.get(4).setEntryPrice(460);
    		}
    	}
    	if(!simulation) {
    		for(Currency c : Main.getInstance().getCurrencies()) {
        		this.wallet.put(c, Double.valueOf(CurrentAPI.get().getAccount().getAssetBalance(c.getName()).getFree()));
        	}
    	}
    }

    public void updateUSDT() {
    	if(!simulation) {
    		USDT = Double.parseDouble(CurrentAPI.get().getAccount().getAssetBalance("USDT").getFree());
    	}
    	
    }
    
    public void getAccountStats() {
    	System.out.println("Account overview:");
    	for(Currency c : Main.getInstance().getCurrencies()) {
    		System.out.println(getPercentOfWallet(c) + "% - " + wallet.get(c) + c.getName() + " (" + (wallet.get(c)*c.getPrice())+"$)");
    	}
    	System.out.println("-----------------");
    }
    
    public double getUSDT() {
		return USDT;
	}
    public double getWalletTotalValue() {
    	return getCoinsTotalValue()+getUSDT();
    }
    
    public Account getRealAccount() {
        return realAccount;
    }



    public void setStartingValue(double startingValue) {
        this.startingValue = startingValue;
    }


    //All the get methods.
    public String getUsername() {
        return username;
    }


    public double getCoinsTotalValue() {
        double value = 0;
        for (Map.Entry<Currency, Double> entry : wallet.entrySet()) {
            Currency currency = entry.getKey();
            Double amount = entry.getValue();
            value += amount * currency.getPrice();
        }
        return value ;
    }
    
    public double getPercentOfWallet(Currency c) {
    	return getWallet().get(c) == 0 ? 0 : ((getWallet().get(c)*c.getPrice())/getWalletTotalValue());
    }

    /**
     * Method has backend.Currency names as keys and the amount of certain currency as value.
     * i.e {"BTCUSDT : 3.23}
     *
     * @return
     */
    public ConcurrentHashMap<Currency, Double> getWallet() {
        return wallet;
    }

    /**
     * Method will calculate current profit off of all the active trades
     *
     * @return returns the sum of all the percentages wether the profit is below 0 or above.
     */
    public double getProfit() {
        return (getWalletTotalValue()/ startingValue)*100;
    }
    
    public String getYieldString() {
    	return "(from " + getStartingValue() + "$ to " + getWalletTotalValue() + "$)";
    }


    //All wallet methods

    /**
     * Method allows to add currencies to wallet hashmap.
     *
     * @param key   Should be the name of the currency ie "BTCUSDT"
     * @param value The amount how much was bought.
     */
    public void addToWallet(Currency key, double value) {
        if (wallet.containsKey(key)) {
            wallet.put(key, wallet.get(key) + value);
        } else {
            wallet.put(key, value);
        }

    }

    /**
     * Method allows to remove values from keys.
     **/
    public void removeFromWallet(Currency key, double value) {
        wallet.put(key, wallet.get(key) - value);
    }

    public double getMakerComission() {
        return makerComission;
    }

    public double getTakerComission() {
        return takerComission;
    }
    
    public double getStartingValue() {
		return startingValue;
	}
    

    public double getBuyerComission() {
        return buyerComission;
    }

	public double getUSDTAvailable() {
		return getUSDT()*PER_CRYPTO_ALLOCATION;
	}
}
