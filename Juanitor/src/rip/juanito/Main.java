
package rip.juanito;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import rip.juanito.config.ConfigSetup;
import rip.juanito.config.Formatter;
import rip.juanito.controller.Controller;
import rip.juanito.trades.BuySell;
import rip.juanito.trades.Currency;
import rip.juanito.trades.LocalAccount;
import rip.juanito.trades.backtest.Backtest;

@SuppressWarnings("restriction")
public class Main extends Application {

	private static Main instance;
	private LocalAccount localAccount;
	private final List<Currency> currencies = new ArrayList<>();
	private static final String BINANCE_API_KEY = "Nyv5pBPMC5RMmmPjPXTRhZHX1IvLp2S5IZhm1rb9qvVm7F9wCaWHG8lIpJkeCLnR";
    private static final String BINANCE_API_PW = "iO0wXX4D2rCZ45J4yZGNPerkFmsJmLWIkYJ5y5oMqCvJTcseTVFu0HPfaWxFl0kJ";
    public Backtest backtest;
    
	public static void main(String[] args) {
		launch();
	}

	public Main() {
		instance = this;
		System.out.println("---Startup...");
		System.out.println("     _   _   _       ___   __   _   _   _____   _____   _____   \n"
				+ "    | | | | | |     /   | |  \\ | | | | |_   _| /  _  \\ |  _  \\  \n"
				+ "    | | | | | |    / /| | |   \\| | | |   | |   | | | | | |_| |  \n"
				+ " _  | | | | | |   / / | | | |\\   | | |   | |   | | | | |  _  /  \n"
				+ "| |_| | | |_| |  / /  | | | | \\  | | |   | |   | |_| | | | \\ \\  \n"
				+ "\\_____/ \\_____/ /_/   |_| |_|  \\_| |_|   |_|   \\_____/ |_|  \\_\\ ");
		start();
		Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				backtest = new Backtest(new Date(2020-1900, 11, 21).getTime());
						backtest.start();
				
			}
		});
		thread.start();

	}

	public void start() {
		try {
			ConfigSetup.readConfig();
		} catch (ExceptionInInitializerError cause) {
			if (cause.getCause() != null) {
				if (cause.getCause().getMessage().toLowerCase().contains("banned")) {
					long bannedTime = Long.parseLong(cause.getCause().getMessage().split("until ")[1].split("\\.")[0]);
					System.out.println("\nIP Banned by Binance API until " + Formatter.formatDate(bannedTime) + " ("
							+ Formatter.formatDuration(bannedTime - System.currentTimeMillis()) + ")");
				}
			} else {
				System.out.println("---Error during startup: ");
				cause.printStackTrace();
			}

		}
		
		localAccount = new LocalAccount(BINANCE_API_KEY, BINANCE_API_PW);

		System.out.println("Can trade: " + localAccount.getRealAccount().isCanTrade());
		System.out.println(localAccount.getMakerComission() + " Maker commission.");
		System.out.println(localAccount.getBuyerComission() + " Buyer commission");
		System.out.println(localAccount.getTakerComission() + " Taker comission");
		BuySell.setAccount(localAccount);
		for (String s : ConfigSetup.getCurrencies()) {
			System.out.println("Loading " + s);
			Currency c = new Currency(s, true);
			getCurrencies().add(c);

		}
		localAccount.initAccount();

		new Timer().scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				if (System.currentTimeMillis() - lastWalletUpdate > 60 * 1000 * 3) {
					localAccount.updateUSDT();
					localAccount.updateWallet();
					lastWalletUpdate = System.currentTimeMillis();
					localAccount.getAccountStats();
				}
				

				controller.update();

			}
		}, 1000, 500);
	}

	private long lastWalletUpdate = System.currentTimeMillis()+1000;

	public LocalAccount getLocalAccount() {
		return localAccount;
	}

	@Override
	public void start(Stage primaryStage) {

		primaryStage.initStyle(StageStyle.UNDECORATED);
		init(primaryStage, "app.fxml");

	}

	private static double xOffset = 0;
	private static double yOffset = 0;
	private static boolean clicked = false;
	public static Controller controller;

	public void init(Stage primaryStage, String FXML) {
		try {

			FXMLLoader fxmlLoader = new FXMLLoader();

			Parent root = fxmlLoader.load(instance.getClass().getResource(FXML));

			root.setOnMousePressed(event -> {
				if (event.getSceneY() < 50) {
					xOffset = event.getSceneX();
					yOffset = event.getSceneY();
					clicked = true;
				} else {
					clicked = false;
				}

			});
			root.setOnMouseDragExited(event -> {
				clicked = false;
			});
			// move around here
			root.setOnMouseDragged(event -> {
				if (clicked) {
					primaryStage.setX(event.getScreenX() - xOffset);
					primaryStage.setY(event.getScreenY() - yOffset);
				}

			});
			Scene scene = new Scene(root);
			scene.setFill(Color.TRANSPARENT);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static Main getInstance() {
		return instance;
	}

	public List<Currency> getCurrencies() {
		return currencies;
	}

}
